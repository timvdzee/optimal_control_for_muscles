function qd = act_Umberger_v2(q,STIM,parms)

tau_act = parms.tau_act; 
tau_deact = parms.tau_deact;

var = STIM > q;

tau = var * tau_act + (1 - var) * tau_deact;

qd = (STIM-q)./tau;

% if STIM >= q
%     
%     
%     qd = (STIM - q) ./ tau_act;
% else
%     qd = (STIM - q) ./ tau_deact;
% end

%% Make sure outputs are columns
qd = qd(:);

return