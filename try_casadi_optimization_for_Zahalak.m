clear; close all; clc;
import casadi.*

%% Parameter values
% Zahalak parameters
parms.xi = -1:.01:2;
parms.alpha = 1;
parms.r = 1;
tspan = [0 3];
parms.f1 = 50;
parms.g1 = 14;
parms.g2 = 600;
parms.g3 = 50;
FTfact = 1.7; % difference in rate constants between slow and fast twitch
parms.mu = .01;

% error parameter (in Zahalak function, but not in original) 
parms.eps = 1e-5;

% optimization parameters
N = 100; % nodes
N_iter = 500; % max. number of iterations
slow_twitch = false;

%% Desired force
% load force
load('FastTwitch_1AP_250ms.mat')

% correct force for slow-twitch (because of weird baseline force)
if slow_twitch
    load('SlowTwitch_1AP_250ms.mat')
    Force = Force - 0.5629;
    Force(1) = Force(2);
else % increase rate constants
    parms.f1 = parms.f1 * FTfact;
    parms.g1 = parms.g1 * FTfact;
    parms.g2 = parms.g2 * FTfact;
    parms.g3 = parms.g3 * FTfact;
end
% resample force
t = linspace(0, Time(end)/1000, N);
F_des = interp1(Time/1000, Force/100, t);
CaTropi = interp1(Time/1000, CaTrop/120, t);
dt = mean(diff(t)); % ms

% figure;
% plot(Time/1000,Force/100); hold on
% plot(t, F_des,'o')

%% Set-up Casadi
% Optimization problem
opti = casadi.Opti();

% variables
K = opti.variable(1,N+1); % first state: stiffness K
F = opti.variable(1,N+1); % second state: force F
E = opti.variable(1,N+1); % third state: energy E
R = opti.variable(1,N);   % control: activation R

% initialize cost function
J = 0; 

% loop over control intervals
for k=1:N 
    
   % state derivative
   [dKdt, dFdt, dEdt] = zahalak_opt_func(K(1,k),F(1,k),E(1,k), R(:,k), parms);
   
   % state(k+1) = state(k) + change in state
   opti.subject_to(K(1,k+1) == K(1,k) + (dt * dKdt));
   opti.subject_to(F(1,k+1) == F(1,k) + (dt * dFdt));
   opti.subject_to(E(1,k+1) == E(1,k) + (dt * dEdt));
   
   % cost function: difference in force
   J = J + (F(1,k) - F_des(k)).^2;
end

% cost function: SS difference in force
opti.minimize(J); 

%% initial conditions/guess
Q0 = .05; Q1 = 0; Q2 = .05;
p = Q1./(Q0+eps); % Eq. 52
q = sqrt(max(Q2 .* Q0 - Q1.^2,eps)) ./ (Q0 + eps); % alternative formulation
n = Q0 ./ sqrt(2*pi*q) .* exp(-((parms.xi-p).^2) / (2.*q.^2));  % Eq. 52
% close all
% figure
% plot(parms.xi, n);
% axis([-1 2 0 1.5])

% constraint(s)
opti.subject_to(K(1,1) == Q0);
opti.subject_to(F(1,1) == Q1);
opti.subject_to(E(1,1) == Q2);

opti.subject_to(K>0);
opti.subject_to(E>0);  
% opti.subject_to(F>0); 

% constraints: excitation between 0 and 1
opti.subject_to(R(1)==0);   % start at 0
opti.subject_to(0 <= R <= 1);
  
 % set numerical backend
 p_opts = struct('expand',true);
s_opts = struct('max_iter', N_iter);
opti.solver('ipopt',p_opts, s_opts);

% actual solve!
sol = opti.solve();

%% Plotting
close all
% retrieve force and excitation
Fstar = opti.debug.value(F);
Kstar = opti.debug.value(K);
Estar = opti.debug.value(E);
Cstar = opti.debug.value(R);

% retrieve activation factor R
Rstar = Cstar ./ (Cstar + parms.mu);

figure(1)

% stim
subplot(131);
plot(t,Cstar(1,1:N),'linewidth',2); hold on
plot(Time/1000, CaFree/max(CaFree),'-','linewidth',2);
xlabel('Time (s)')
ylabel('Activator (a.u.)');
title('Activator')

% active state / CaTrop
subplot(132);
plot(t,Rstar(1,1:N),'linewidth',2); hold on
plot(Time/1000, CaTrop/120,'-','linewidth',2);
xlabel('Time (s)')
ylabel('Activation (a.u.)');
legend('Produced','Desired','location','best')
title('Activation')

% force
subplot(133);
plot(t,Fstar(1,1:N),'linewidth',2); hold on
plot(t, F_des,'--','linewidth',2);
xlabel('Time (s)')
ylabel('Force (a.u.)');
legend('Produced','Desired','location','best')
title('Muscle force')

for i = 1:3
    subplot(1,3,i);
    xlim([0 max(t)])
    ylim([0 1])
end

set(gcf,'Position',[100 100 1000 300])

%% plot states
figure(2)

% stim
subplot(131);
plot(t,Kstar(1:N),'linewidth',2); hold on
xlabel('Time (s)')
ylabel('Stiffness (a.u.)');
title('Stiffness')

% active state / CaTrop
subplot(132);
plot(t,Fstar(1:N),'linewidth',2); hold on
xlabel('Time (s)')
ylabel('Force (a.u.)');
title('Force')

% force
subplot(133);
plot(t,Estar(1:N),'linewidth',2); hold on
xlabel('Time (s)')
ylabel('Energy (a.u.)');

title('Energy')

for i = 1:3
    subplot(1,3,i);
    xlim([0 max(t)])
%     ylim([0 1])
end

set(gcf,'Position',[100 100 1000 300])

%% test on original zahalak function
% n_check = nan((length(Fstar)-1), length(parms.xi));
% 
% for i = 1:(length(Fstar)-1)
%     parms.r = Cstar(i);
%     [~,n_check(i,:)] = zahalak_func(t(i), [Kstar(i) Fstar(i) Estar(i)], parms);
% end

%% retrieve n(x)
eps = parms.eps;
figure(3)

nstar = nan(length(Fstar), length(parms.xi));

for i = 1:(length(Fstar)-1)
    p = Fstar(i)./(Kstar(i)+eps); % Eq. 52
    q = sqrt(max(Estar(i) .* Kstar(i) - Fstar(i).^2,eps)) ./ (Kstar(i) + eps); % alternative formulation

    nstar(i,:) = Kstar(i) ./ sqrt(2*pi*q) .* exp(-((parms.xi-p).^2) / (2.*q.^2));  % Eq. 52

    % plot
    plot([-1 2], [1 1], 'k--'); hold on
    plot([0 0], [0 1.2], 'k--')
    set(gca, 'ylim', [0 1.2])
    xlabel('Position x (a.u.)')
    ylabel('Attached XBs n (a.u.)');
    title([num2str(t(i))]);
    
    h1 = line('xdata',parms.xi,'ydata',nstar(i,:),'linewidth',2); 
%     h2 = line('xdata',parms.xi,'ydata',n_check(i,:),'linestyle','--','color','red','linewidth',2); 
    
    drawnow
    figure(3)
    
    if i ~= (N-1)
        delete(h1);
%         delete(h2);
    else, break
    end
end

%% Zahalak function
function[Qd1,Qd2,Qd3] = zahalak_opt_func(Q0,Q1,Q2, C, parms)
% based on Zahalak & Ma (1990), J. Biomech. Eng.
    
% retrieve parameters
alpha = parms.alpha;
xi = parms.xi;
f1 = parms.f1;
g1 = parms.g1;
g2 = parms.g2;
g3 = parms.g3;
eps = parms.eps;
mu = parms.mu;

% alpha = parms.alpha * .5*sin(2*pi*t*f) + .5;

% attachment and dettachment functions
f = @(xi,f1)     xi .* f1 .* (xi>0 & xi<1);
g = @(xi,g1,g2,g3)  xi .* g1 .* (xi>0 & xi<1) + g2 .* (xi<0) + (g3 + g3 .* (xi-1)) .* (xi>1);

% loop over states
% betha = nan(3,1); phi = nan(3,1); Qd = nan(3,1);
for lambda = 0:2   
   
    p = Q1./(Q0+eps); % Eq. 52
%     q = sqrt(Q2./(Q0+.001) - (Q1./(Q0+.001)).^2);  % Eq. 52
    q = sqrt(max(Q2 .* Q0 - Q1.^2, eps)) ./ (Q0 + eps); % alternative formulation
    
    n = Q0 ./ sqrt(2*pi*q) .* exp(-((xi-p).^2) / (2.*q.^2));  % Eq. 52
    
    betha = trapz(xi, xi.^lambda .* f(xi,f1));   % Eq. 48
    phi   = trapz(xi, [xi.^lambda .* f(xi,f1) .* n]') + trapz(xi, [xi.^lambda .* g(xi,g1,g2,g3) .* n]');   % Eq. 49+50
    
    % calcium dynamics
    r = C ./ (C+mu);
    
    if lambda == 0, Qd1 = alpha * r * betha - phi;    % Eq. 46
    elseif lambda == 1, Qd2 = alpha * r * betha - phi;    % Eq. 46
    elseif lambda == 2, Qd3 = alpha * r * betha - phi;    % Eq. 46
    end
end

% y = [n(:)];

end

%% zahalak function
function[Qd,y] = zahalak_func(t, x, parms)
% based on Zahalak & Ma (1990), J. Biomech. Eng.
    
% disp(t);

% retrieve states
Q0 = x(1);
Q1 = x(2);
Q2 = x(3);

% retrieve parameters
% f = parms.f;
c = parms.c;
r = parms.r;
xi = parms.xi;
f1 = parms.f1;
g1 = parms.g1;
g2 = parms.g2;
g3 = parms.g3;
alpha = parms.alpha;

% alpha = parms.alpha * .5*sin(2*pi*t*f) + .5;

% attachment and dettachment functions
f = @(xi,f1)     xi .* f1 .* (xi>0 & xi<1);
g = @(xi,g1,g2,g3)  xi .* g1 .* (xi>0 & xi<1) + g2 .* (xi<0) + (g3 + g3 .* (xi-1)) .* (xi>1);

% loop over states
betha = nan(3,1); phi = nan(3,1); Qd = nan(3,1);
for lambda = 0:2   
    p = Q1/Q0; % Eq. 52
    q = sqrt(Q2/Q0 - (Q1/Q0)^2);  % Eq. 52
        
    n = Q0 ./ sqrt(2*pi*q) * exp(-((xi-p).^2) / (2*q^2));  % Eq. 52
    
    betha(lambda+1,1) = trapz(xi, xi.^lambda .* f(xi,f1));   % Eq. 48
    phi(lambda+1,1)   = trapz(xi, xi.^lambda .* f(xi,f1) .* n) + trapz(xi, xi.^lambda .* g(xi,g1,g2,g3) .* n);   % Eq. 49+50
    Qd(lambda+1,1) = alpha * r * betha(lambda+1,1) - phi(lambda+1,1);    % Eq. 46
end

y = [n(:)];

end
