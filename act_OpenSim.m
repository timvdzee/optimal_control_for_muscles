function qd = act_OpenSim(q,STIM,parms)

tau_act = parms.tau_act; 
tau_deact = parms.tau_deact;

qd = (STIM-q)./ (((STIM > q) .* tau_act + (1 - (STIM > q)) .* tau_deact) .* (.5+1.5*q));

%% Make sure outputs are columns
qd = qd(:);

return