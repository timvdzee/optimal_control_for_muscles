clear; close all; clc;
import casadi.*

%% Hill-type model parameters
% time constants
tau_act = .015; % s
tau_deact = .05; % s

% slow-twitch
parms.tau_act = [.01; .07];
parms.tau_deact = [.4; .01];

% % fast-twitch
parms.tau_act = [.01; .02];
parms.tau_deact = [.1; .01];

%% Desired force
% load force
% load('SlowTwitch_1AP_250ms.mat')
load('FastTwitch_1AP_250ms.mat')
% F_des = Force/100;
% t = Time / 1000; % ms -> s

% resample
N = 100;
t = linspace(0, Time(end)/1000, N);
F_des = interp1(Time/1000, Force/100, t);

dt = mean(diff(t)); % ms
F_des(1) = F_des(2);

%% Set-up Casadi
% Optimization problem
opti = casadi.Opti();

% variables
X = opti.variable(2,N+1); % state trajectory
U = opti.variable(1,N);   % control trajectory (STIM)

% state-derivative function
f = @(x,u,tau_act,tau_deact) [(u-x(1,:))./ (((u > x(1,:)) .* tau_act(1) + (1 - (u > x(1,:))) .* tau_deact(1)) .* (.5+1.5*x(1,:))); ...
                              (x(1,:)-x(2,:))./ (((x(1,:) > x(2,:)) .* tau_act(2) + (1 - (x(1,:) > x(2,:))) .* tau_deact(2)))]; 

% initialize cost function
J = 0; 
c = [1e-6 1e-3];

% loop over control intervals
for k=1:N 
    
   % state derivative
   dXdt = f(X(:,k), U(:,k), parms.tau_act, parms.tau_deact);
   
   % state(k+1) = state(k) + change in state
   opti.subject_to(X(1,k+1) == X(1,k) + (dt * dXdt(1)));
   opti.subject_to(X(2,k+1) == X(2,k) + (dt * dXdt(2)));
   
   % cost function: difference in force
   J = J + (X(2,k) - F_des(k)).^2 + c(1)*(dXdt(1).^2) + c(2)*(X(1,k).^2);
end

% cost function: SS difference in force
opti.minimize(J); 

% constraints: excitation between 0 and 1
opti.subject_to(X(1,1)==0);   % start at 0
opti.subject_to(X(2,1)==0);   % start at 0
opti.subject_to(U(1)==0);   % start at 0
opti.subject_to(0 <= U <= 1);
  
 % set numerical backend
opti.solver('ipopt');

% actual solve
sol = opti.solve();

% retrieve force and excitation
Xstar = sol.value(X);
Ustar = sol.value(U);


%% Plotting
% % in case of fail
Xstar = opti.debug.value(X);
Ustar = opti.debug.value(U);

close all
figure

% stim
subplot(131);
plot(t(1:N),Ustar,'linewidth',2); hold on
plot(Time/1000, CaFree/max(CaFree),'-','linewidth',2);
xlabel('Time (s)')
ylabel('Activator (a.u.)');
title('Activator')

% active state / CaTrop
subplot(132);
plot(t,Xstar(1,1:N),'linewidth',2); hold on
plot(Time/1000, CaTrop/120,'-','linewidth',2);
xlabel('Time (s)')
ylabel('Activation (a.u.)');
legend('Produced','Desired','location','best')
title('Activation')

% force
subplot(133);
plot(t,Xstar(2,1:N),'linewidth',2); hold on
plot(t, F_des,'-','linewidth',2);
xlabel('Time (s)')
ylabel('Force (a.u.)');
legend('Produced','Desired','location','best')
title('Muscle force')

% % force-rate
% subplot(133);
% plot(t(1:N), diff(Fstar)/dt,'linewidth',2); hold on
% plot(t(1:N), f(Fstar(1:N),Ustar,tau),'-','linewidth',2); hold on
% xlabel('Time (s)')
% ylabel('Force-rate (a.u.)');
% legend('Force derivative','From STIM','location','best')
% title('Muscle force-rate')

for i = 1:3
    subplot(1,3,i);
    xlim([0 max(t)])
    ylim([0 1])
end

set(gcf,'Position',[100 100 1000 300])
