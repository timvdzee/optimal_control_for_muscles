% script file to run the 1998 curtin activation model, with some minor
% improvements and additional features. 

% in general, there are two ways to go in modeling the muscle activation
% dynamics. 1) take all parameters from physiological experiments, ie
% directly measure the movement of calcium ions and such and model
% that, or 2) device a model and fit the parameters pertaining to that
% model on measurement of muscle forces. The problem with 1) is that the in
% vivo behaviour is very difficult to capture, the problem with 2) is that
% you may want to also fit other parameters, for instance those pertaining
% to contraction kinetics, on these force records. If this is the case then
% there's an interaction between these parameters because you're fitting
% two dynamical systems in series on a single dynamical response. Note that
% the time constants of those dynamical systems are in the same ballpark
% (otherwise we wouldn't have to bother with both of them anyway...). 

clear all
close all
clc
% first set of parms in curtin 1998, these were fitted on data collected
% from lamprey fish at <10 degrees Celsius.
parms.n=2.55; % []
parms.Km=0.271; % []
parms.tau_act=0.114; % [s]
parms.tau_deact=parms.tau_act;
gamma0=0;

tspan=[0 0.89];
odeopt=odeset('abstol',1e-10,'reltol',1e-10);

[t,gamma]=ode45(@curtin_1998_activation_dyn,tspan,gamma0,odeopt,parms);
XA=curtin_1998_active_state( gamma,parms);

figure;
subplot(311)
plot(t,gamma,'k'); hold on
plot([.1 .1],[0 1],'k--')
plot([.39 .39],[0 1],'k--')
xlabel('time [s]')
ylabel('free calcium a (relative units)')
subplot(313)
plot(t,XA,'k'); hold on
plot([.1 .1],[0 1],'k--')
plot([.39 .39],[0 1],'k--')
xlabel('time [s]')
ylabel('XA (relative units)')
subplot(312)
gamma=0:.001:1;
XA=curtin_1998_active_state( gamma,parms);
plot(gamma,XA,'k'); hold on
ylabel('XA (relative units)')
xlabel('free calcium a (relative units)')


% second set of parms in curtin 1998
parms.tau_act=0.150; % [s]
parms.tau_deact=parms.tau_act;

[t,gamma]=ode45(@curtin_1998_activation_dyn,tspan,gamma0,odeopt,parms);
gamma(gamma<0)=0;
XA=curtin_1998_active_state( gamma,parms);

subplot(311)
plot(t,gamma,'k--')
legend('tau = 0.114','tau = 0.150')
subplot(313)
plot(t,XA,'k--')
legend('tau = 0.114','tau = 0.150')

function [ q ] = curtin_1998_active_state( gamma,parms)
%function [ q ] = curtin_1998_active_state( gamma,parms )
%   input: free calcium concentration gamma and parms
%   output: active state q
% formula according to Curtin (1998), with the first term added such that
% if q=f(gamma), then f(1)=1 (this wasn't the case in the original..)

% koen lemaire 3/2020
n=parms.n; 
k=parms.Km; 
% original curtin 1998 equation:
%q = (gamma.^n)./(gamma.^n + k.^n);
% new implementation such that f(1)=1
q = (1+k.^n).*(gamma.^n)./(gamma.^n + k.^n);

end

function [ gammad ] = curtin_1998_activation_dyn( t,gamma,parms )
%function [ gammad ] = curtin_1998_activation_dyn( t,gamma,parms )
%   input: time t,free calcium concentration gamma, parms
%   output: time derivative of gamma
% in comparison to curtin 1998 this function allows for variable
% stimulation and allows for different time constants for activation and
% deactivation

% koen lemaire 3/2020
gamma = gamma(:);
% read out parameters
tau_act=parms.tau_act;
tau_deact=parms.tau_deact;

% define muscle stimulation; values below to reproduce curtin (1998) 
if t>.1 && t<.39
    stim=1;
else
    stim=0;
end

% calculation of derivative
a=stim>=gamma; % ascending part
d=~a; % descending part
gammad(a) = (stim(a)-gamma(a))/tau_act;
gammad(d) = (stim(d)-gamma(d))/tau_deact;
end