clear; close all; clc;
import casadi.*

%%
% desired force
load('SlowTwitch_1AP_250ms.mat')
% F_des = (A/2)*cos(2*pi*freq*t + pi) + A/2;
F_des = Force/100;
t = Time;
N = length(t);
dt = mean(diff(t));

F_des(1) = F_des(2);

% parameters
% N = 100; % number of control intervals
tau = .015; % s
% T = 3; % s
% dt = T/N; % length of a control interval
% t = linspace(0,T,N+1);
% A = .9;
% freq = .5; % Hz

 % Optimization problem
opti = casadi.Opti();

% variables
X = opti.variable(2,N+1); % state trajectory
F = X(1,:);
U = opti.variable(1,N);   % control trajectory (STIM)

% state-derivative function
f = @(x,u,tau) (u-x(1,:))/(tau*(.5+1.5*x(1,:))); % dx/dt = f(x,u)
% f = @(x,u,tau) (u-x(1,:))/(tau); % dx/dt = f(x,u)

% initialize cost function
J = 0; 

% loop over control intervals
for k=1:N 
    
   dFdt = f(X(:,k),         U(:,k), tau);
   
   X_next = X(:,k) + (dt * dFdt);
   
   opti.subject_to(X(:,k+1) == X_next); % close the gaps
   
   % cost function: difference in force
   J = J + (X(1,k) - F_des(k)).^2;
end

opti.minimize(J); % minize difference in force

% constraints
opti.subject_to(0 <= U <= 1);           % control is limited
% opti.subject_to(Fdot(1)== 0);    
opti.subject_to(F(1)== F_des(1));    
opti.subject_to(F(N+1)== F_des(end));    

% initial guess
opti.set_initial(F, 0);
opti.set_initial(U, 0);
% opti.set_initial(Fdot, 1);

opti.solver('ipopt'); % set numerical backend
sol = opti.solve();   % actual solve

Fstar = sol.value(F);
Ustar = sol.value(U);

%% Plotting
close all
figure

% stim
subplot(131);
plot(t(1:N),Ustar,'linewidth',2); hold on
plot(t, CaFree/max(CaFree),'-','linewidth',2);
xlabel('Time (s)')
ylabel('Activator (a.u.)');
title('Activator')

% active state / CaTrop
subplot(132);
plot(t,Fstar(1:N),'linewidth',2); hold on
plot(t, CaTrop/120,'-','linewidth',2);
xlabel('Time (s)')
ylabel('Activation (a.u.)');
legend('Produced','Desired','location','best')
title('Activation')

% force
subplot(133);
plot(t,Fstar(1:N),'linewidth',2); hold on
plot(t, F_des,'--','linewidth',2);
xlabel('Time (s)')
ylabel('Force (a.u.)');
legend('Produced','Desired','location','best')
title('Muscle force')

% % force-rate
% subplot(133);
% plot(t(1:N), diff(Fstar)/dt,'linewidth',2); hold on
% plot(t(1:N), f(Fstar(1:N),Ustar,tau),'-','linewidth',2); hold on
% xlabel('Time (s)')
% ylabel('Force-rate (a.u.)');
% legend('Force derivative','From STIM','location','best')
% title('Muscle force-rate')

for i = 1:3
    subplot(1,3,i);
    xlim([0 max(t)])
end


set(gcf,'Position',[100 100 1000 300])
