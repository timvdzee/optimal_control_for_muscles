function [dndt] = dndt(t,n,parms)

% determine f and g as function of x
[f,g] = fxgx(parms.x, parms);

% make column
f = f(:);
g = g(:);

a = interp1(parms.t, parms.a, t);

% zahalak equation
dndt = a.*f.*(1-n) - g.*n;

dFdt = parms.x * trapz(dndt,parms.x); 

end

